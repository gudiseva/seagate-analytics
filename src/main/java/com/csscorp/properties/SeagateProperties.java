package com.csscorp.properties;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SeagateProperties {

	static Logger logger = LoggerFactory.getLogger(SeagateProperties.class);

	protected static Properties getProperties() throws IOException {

		Properties props = new Properties();

		String propertiesFile = "config.properties";
		InputStream inStream = SeagateProperties.class.getClassLoader().getResourceAsStream(propertiesFile);
				
		if (inStream != null){
			props.load(inStream);
		}
		else {
			throw new FileNotFoundException("Property file not found!");
		}

		return props;
	}

    public static String getAmazonURL() throws IOException {
    	return getProperties().getProperty("AMAZON.URL");
	}
    
    public static String getAmazonProduct() throws IOException {
    	return getProperties().getProperty("AMAZON.PRODUCT");
	}
    
    public static String getAmazonOutputPath() throws IOException {
    	return getProperties().getProperty("AMAZON.OUTPUT.PATH");
	}
    
    public static String getAmazonProject() throws IOException {
    	return getProperties().getProperty("AMAZON.PROJECT");
	}

    public static String getBestBuyURL() throws IOException {
    	return getProperties().getProperty("BESTBUY.URL");
	}

    public static String getBestBuyProduct() throws IOException {
    	return getProperties().getProperty("BESTBUY.PRODUCT");
	}

    public static String getBestBuyOutputPath() throws IOException {
    	return getProperties().getProperty("BESTBUY.OUTPUT.PATH");
	}

    public static String getBestBuyProject() throws IOException {
    	return getProperties().getProperty("BESTBUY.PROJECT");
	}

    public static String getEBayURL() throws IOException {
    	return getProperties().getProperty("EBAY.URL");
	}
    
    public static String getEBayProduct() throws IOException {
    	return getProperties().getProperty("EBAY.PRODUCT");
	}
    
    public static String getEBayOutputPath() throws IOException {
    	return getProperties().getProperty("EBAY.OUTPUT.PATH");
	}
    
    public static String getEBayProject() throws IOException {
    	return getProperties().getProperty("EBAY.PROJECT");
	}

    public static String getMouthShutURL() throws IOException {
    	return getProperties().getProperty("MOUTHSHUT.URL");
	}

    public static String getMouthShutOutputPath() throws IOException {
    	return getProperties().getProperty("MOUTHSHUT.OUTPUT.PATH");
	}

    public static int getMouthShutMaxPages() throws IOException {
    	return Integer.parseInt(getProperties().getProperty("MOUTHSHUT.MAX.PAGES"));
	}

    public static String getConsumerAffairsURL() throws IOException {
    	return getProperties().getProperty("CONSUMER.AFFAIRS.URL");
	}

    public static String getConsumerAffairsOutputPath() throws IOException {
    	return getProperties().getProperty("CONSUMER.AFFAIRS.OUTPUT.PATH");
	}

    public static int getConsumerAffairsMaxPages() throws IOException {
    	return Integer.parseInt(getProperties().getProperty("CONSUMER.AFFAIRS.MAX.PAGES"));
	}

    public static String getHissingKittyURL() throws IOException {
    	return getProperties().getProperty("HISSING.KITTY.URL");
	}

    public static String getHissingKittyOutputPath() throws IOException {
    	return getProperties().getProperty("HISSING.KITTY.OUTPUT.PATH");
	}

    public static int getHissingKittyMaxPages() throws IOException {
    	return Integer.parseInt(getProperties().getProperty("HISSING.KITTY.MAX.PAGES"));
	}

}