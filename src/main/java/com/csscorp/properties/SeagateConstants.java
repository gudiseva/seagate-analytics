package com.csscorp.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SeagateConstants {

	static Logger logger = LoggerFactory.getLogger(SeagateConstants.class);

	public static final int MILLIS_IN_DAY = 1000 * 60 * 60 * 24;

}
