package com.csscorp.properties;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SeagateHelper {

	static Logger logger = LoggerFactory.getLogger(SeagateHelper.class);

    /**
     * Creates a folder to desired location if it not already exists
     * @param dirName - full path to the folder
     * @throws SecurityException - in case you don't have permission to create the folder
     */
	public static void createFolderIfNotExists(String directoryName) throws SecurityException {
    	File directory = new File(directoryName);
    	if (!directory.exists()) {
    		directory.mkdir();
    	}
    }

	public static void createFolders(String filePathString) throws IOException {
    	
    	Path filePath = Paths.get(filePathString);
    	
        if (!Files.exists(filePath))
        	Files.createDirectories(filePath);
        
    }
	
	/*
	 * Note: StringTokenizer is a legacy class, retained for compatibility reasons, the use is discouraged! Please use string.split().
	 */
	public static String ratingRegexExtractor(String content){
		
		Pattern pattern = Pattern.compile("[\\d]\\/[\\d]");
		Matcher matcher = pattern.matcher(content);
		if (matcher.find())
		{
			String matchedString = matcher.group(0).trim();
			StringTokenizer token = new StringTokenizer(matchedString, "/");
			return token.nextToken();
		}
		else {
			return null;
		}
	}
	
	
	public static String reverseParsePrettyDate(String prettyStr) throws java.text.ParseException {
		
		Calendar cal = Calendar.getInstance();
		
		String[] prettyArr = prettyStr.split("\\s");

		if (prettyArr[0].matches("^[0-9]*$") && prettyArr[1].equalsIgnoreCase("hr")){
			int hour = Integer.parseInt(prettyArr[0]);
			//String hourStr = prettyArr[1];
			cal.add(Calendar.HOUR, -hour);	
		}

		if (prettyArr[0].matches("^[0-9]*$") && prettyArr[1].equalsIgnoreCase("hrs")){
			int hours = Integer.parseInt(prettyArr[0]);
			//String hoursStr = prettyArr[1];
			cal.add(Calendar.HOUR, -hours);	
		}

		if (prettyArr[2].matches("^[0-9]*$") && prettyArr[3].equalsIgnoreCase("min")){
			int min = Integer.parseInt(prettyArr[2]);
			//String minStr = prettyArr[3];
			cal.add(Calendar.MINUTE, -min);
		}

		if (prettyArr[2].matches("^[0-9]*$") && prettyArr[3].equalsIgnoreCase("mins")){
			int mins = Integer.parseInt(prettyArr[2]);
			//String minsStr = prettyArr[3];
			cal.add(Calendar.MINUTE, -mins);
		}

		if (prettyArr[0].matches("^[0-9]*$") && prettyArr[1].equalsIgnoreCase("day")){
			int day = Integer.parseInt(prettyArr[0]);
			//String dayStr = prettyArr[1];
			cal.add(Calendar.DAY_OF_MONTH, -day);	
		}

		if (prettyArr[0].matches("^[0-9]*$") && prettyArr[1].equalsIgnoreCase("days")){
			int days = Integer.parseInt(prettyArr[0]);
			//String daysStr = prettyArr[1];
			cal.add(Calendar.DAY_OF_MONTH, -days);	
		}

		Date convertedTime = cal.getTime();
		String convertedStr = convertedTime.toString(); // "Fri Apr 21 16:59:36 IST 2017";
		DateFormat readFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
		
	    DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd");
	    Date date = readFormat.parse(convertedStr);

	    String formattedDate = writeFormat.format(date);
	    return formattedDate;
	    
	}

}