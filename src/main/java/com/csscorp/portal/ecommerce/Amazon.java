package com.csscorp.portal.ecommerce;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csscorp.properties.SeagateHelper;
import com.csscorp.properties.SeagateProperties;

public class Amazon {

	private static Logger logger = LoggerFactory.getLogger(Amazon.class);
	private static final SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
	private static final SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	public Amazon() throws IOException {
		logger.info("------------ Seagate AMAZON Web Scraping :: [Start] :: --------------");
		long startTime = System.nanoTime();
		webScraper();
		long endTime   = System.nanoTime();
		long totalTime = endTime - startTime;
		logger.info("Elapsed time in milliseconds: " + totalTime / 1000000);
		logger.info("------------ Seagate AMAZON Web Scraping :: [End] :: --------------");
	}


	private static void webScraper() throws IOException {
		
		String productCategory = SeagateProperties.getAmazonProduct();
		String strUrl = SeagateProperties.getAmazonURL();
		String strFolderPath = SeagateProperties.getAmazonOutputPath();
		String strProjectName = SeagateProperties.getAmazonProject();
		
		int pageId = 0;

		String reviewDate = null;

		int startRange = 1;

		String strFilePath = null;

		String strTimeStamp = timeStampFormat.format(new Date());

		String directoryName = strFolderPath + strProjectName;
		SeagateHelper.createFolders(directoryName);
		strFilePath = directoryName + "//" + productCategory + "_amazon_" + strTimeStamp + ".tsv";
		BufferedWriter buffWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(strFilePath,true)));
		
		String url = strUrl + 1;
		logger.info("URL: " + url);
		Document trailDoc = null;
		String totalReveiwCount = "0";

		do
		{
			try {
				trailDoc = Jsoup.connect(url).timeout(0).get();
				totalReveiwCount = trailDoc.select("span[class=a-size-medium totalReviewCount]").first().text();
				logger.info("TotalReviews: " + totalReveiwCount);
				totalReveiwCount = totalReveiwCount.replace(",", "");
			} catch(HttpStatusException ex){

			try {
					trailDoc = Jsoup.connect(url).timeout(0).get();
					totalReveiwCount = trailDoc.select("span[class=a-size-medium totalReviewCount]").first().text();
					logger.info("TotalReviews: " + totalReveiwCount);
					totalReveiwCount = totalReveiwCount.replace(",", "");
				} catch(HttpStatusException exception){
					logger.error("Exception: ", exception);
				}
			}
			catch(SocketTimeoutException ex){
				try {
					trailDoc = Jsoup.connect(url).timeout(0).get();
					totalReveiwCount = trailDoc.select("span[class=a-size-medium totalReviewCount]").first().text();
					logger.info("TotalReviews: " + totalReveiwCount);
					totalReveiwCount = totalReveiwCount.replace(",", "");
				} catch(HttpStatusException exception){
					logger.error("Exception: ", exception);
				}
			}
		} while(Integer.parseInt(totalReveiwCount) <= 1);

		int max_pages = Integer.parseInt(totalReveiwCount);

		pageId = max_pages;

		max_pages = (max_pages / 10) + 1;
		//max_pages = 62; //TBD;

		logger.info("max_pages: " + max_pages);

		try 
		{
			for (int i = startRange; i <=max_pages; i++)
			{
				url = strUrl + i;
				logger.info("URL: " + url);
				Document doc = null;

				try {
					Thread.sleep(3000);  //1000 milliseconds is one second.
					doc = Jsoup.connect(url).timeout(0).get();;
				} catch(InterruptedException ex) {
					i = i - 1;
					continue;
				}
				catch(HttpStatusException ex){
					i = i - 1;
					continue;
				}
				catch(SocketTimeoutException ex){
					i = i - 1;
					continue;
				}
				catch(UnknownHostException ex){
					i = i - 1;
					continue;
				}


				Elements alldiv = doc.select("div[class]");

				for(Element baseElement : alldiv)
				{
					if(baseElement.attr("class").equals("a-section review"))
					{	
						Elements childElements = baseElement.getElementsByAttributeValue("class", "a-row");
						Element reviewElement = baseElement.getElementsByAttributeValue("class", "a-row review-data").first();

						String rating = "",title = "", author = "", time="", reviewText = "";
						int ratingInt = 0;
						for(Element div : childElements)
						{
							Elements spanEle = div.select("span[class]");
							Elements anchorEle = div.select("a[class]");

							for(Element ele : spanEle)
							{
								if(ele.attr("class").equals("a-icon-alt"))
									rating = ele.text();

								if(ele.attr("class").equals("a-size-base a-color-secondary review-date"))
									time = ele.text();
							}

							String recArr[];
							recArr = rating.split(" ");
							float ratingFloat = Float.parseFloat(recArr[0]);
							ratingInt = (int) ratingFloat;
							for(Element ele : anchorEle)
							{
								if(ele.attr("class").equals("a-size-base a-link-normal review-title a-color-base a-text-bold"))
									title = ele.text();

								if(ele.attr("class").equals("a-size-base a-link-normal author"))
									author = ele.text();
							}

						}

						time = time.replace("on ","");
						try {
							Date date = sdf.parse(time);
							reviewDate = output.format(date);
						} catch (ParseException exception) {
							logger.error("Exception: ", exception);
						}

						reviewText = reviewElement.getElementsByAttributeValue("class", "a-size-base review-text").text();

						buffWriter.write("amazon.com" + "\t" + i + "\t" + pageId + "\t" + productCategory + "\t" + title + "\t" + "userlocation" + "\t" + reviewDate + "\t" + "IsRecommended" + "\t" + author + "\t" + "gender" + "\t" + reviewText + "\t" + ratingInt + "\n");

						pageId = pageId - 1;
					}

				}

			}	
		} 

		catch (IOException exception) {
			logger.error("Exception: ", exception);
		}
		finally {
			try {
				buffWriter.close();
			} catch (IOException exception) {
				logger.error("Exception: ", exception);
			}
		}
	
	}

}
