package com.csscorp.portal.ecommerce;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.HttpStatusException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csscorp.properties.SeagateHelper;
import com.csscorp.properties.SeagateProperties;

public class BestBuy {

	private static Logger logger = LoggerFactory.getLogger(BestBuy.class);
	
	private static char quoteChar = '"';
	static String product_id = null;
	static BufferedWriter buffWriter = null;
	
	public BestBuy() throws IOException, ParseException {

		logger.info("------------ Seagate BESTBUY Web Scraping :: [Start] :: --------------");
		long startTime = System.nanoTime();
		webScraper();
		long endTime   = System.nanoTime();
		long totalTime = endTime - startTime;
		logger.info("Elapsed time in milliseconds: " + totalTime / 1000000);
		logger.info("------------ Seagate BESTBUY Web Scraping :: [End] :: --------------");

	}
	
	private static void webScraper() throws IOException, ParseException {
		
		String URL = SeagateProperties.getBestBuyURL();
		String strURL = URL;
		String projectFile = SeagateProperties.getBestBuyProject();
		
		String query_offset = "0";
		String query_limit = "1";


		strURL = strURL.replace("my_offset", query_offset);
		strURL = strURL.replace("my_limit", query_limit);

		logger.debug("query_offset : "+ query_offset);
		logger.debug("query_limit : "+ query_limit);
		logger.debug("Modified api : "+strURL);

		long totalResults = 0;
		totalResults = getReviewCount(strURL);
		logger.debug("Totals Results: "+totalResults);

		float review_hits_ceil = ((float)totalResults/(float)100);
		int review_hits = (int) Math.ceil(review_hits_ceil);
		int q_offset = 0;
		query_limit = "100";

		String directoryName = SeagateProperties.getBestBuyOutputPath();
		SeagateHelper.createFolders(directoryName);
		buffWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(directoryName + projectFile + "_bestbuy_" + totalResults + ".tsv")));

		try{
			for (int i = 0; i<review_hits; i++){

				q_offset = i*100;
				query_offset = String.valueOf(q_offset);
				strURL = URL;  
				strURL = strURL.replace("my_offset", query_offset);
				strURL = strURL.replace("my_limit", query_limit);
				logger.debug("query_offset : "+ query_offset);
				logger.debug("query_limit : "+ query_limit);
				logger.debug("Modified api : "+strURL);


				getReviews(strURL,query_offset,query_limit,totalResults);
			}
		}
		catch (Exception exception) {
			logger.error("Exception: ", exception);
		}

		finally {
			try {
				buffWriter.close();
			} catch (IOException exception) {
				logger.error("Exception: ", exception);
			}
		}

	}

	private static void getReviews(String strURL,String query_offset,String query_limit, long totalResults) throws FileNotFoundException, ParseException{

		try {

			StringBuilder sb = new StringBuilder();

			String reviewDate = null;
			
			
			URL url = new URL(strURL);
			URLConnection urlConnection = url.openConnection();
			JSONParser parser = new JSONParser();

			urlConnection.connect();

			// get the current status of the mass request
			InputStream isStatus = urlConnection.getInputStream();
			InputStreamReader isrStatus = new InputStreamReader(isStatus);

			sb.delete(0, sb.length());
			int noCharsRead;
			char[] chArray = new char[1024];
			while ((noCharsRead = isrStatus.read(chArray)) > 0) {
				sb.append(chArray, 0, noCharsRead);
			}

			// parse the json response and fetch the current status of the request
			Object ob = parser.parse(sb.toString());
			JSONObject jsonObj = (JSONObject) ob;

			JSONArray results = (JSONArray) jsonObj.get("Results");

			for(int i = 0; i < results.size(); i++)
			{
				JSONObject obj = (JSONObject) results.get(i);
				JSONObject obj3 = (JSONObject) obj.get("ContextDataValues");
				JSONObject obj4 = null;
				String gender = null;

				if (obj3.containsKey("Gender"))
				{
					obj4 = (JSONObject) obj3.get("Gender");
					gender = (String) obj4.get("Value"); 
				}
				else
					gender = null;

				reviewDate = ((String) obj.get("LastModificationTime")).replace("T"," ");
				reviewDate = reviewDate.replace(".000+00:00","");
				
				buffWriter.write("bestbuy.com" + "\t" +"NULL" + "\t" + obj.get("Id") + "\t" + obj.get("ProductId") + "\t" + obj.get("Title")  + "\t" + obj.get("UserLocation") + "\t" + reviewDate + "\t" +   obj.get("IsRecommended") + "\t" + obj.get("UserNickname") + "\t" +  gender + "\t" + normalizeCsv(obj.get("ReviewText").toString()) + "\t" + obj.get("Rating") + "\n");
			}

		} catch (Exception exception) {
			logger.error("Exception: ", exception);
		}
	}
	
	private static long getReviewCount(String strURL) throws IOException, ParseException
	{
		StringBuilder sb = new StringBuilder();
		URL url = new URL(strURL);
		logger.info("URL: " + url);
		URLConnection urlConnection = url.openConnection();
		JSONParser parser = new JSONParser();

		try {
			urlConnection.connect();
		} catch(UnknownHostException ex){

			try {
				urlConnection.connect();
			} catch(HttpStatusException exception){
				logger.error("Exception: ", exception);
			}
		}

		// get the current status of the mass request
		InputStream isStatus = urlConnection.getInputStream();
		InputStreamReader isrStatus = new InputStreamReader(isStatus);

		sb.delete(0, sb.length());
		int noCharsRead;
		char[] chArray = new char[1024];
		while ((noCharsRead = isrStatus.read(chArray)) > 0) {
			sb.append(chArray, 0, noCharsRead);
		}

		// parse the json response and fetch the current status of the request
		Object ob = parser.parse(sb.toString());
		
		JSONObject jsonObj = (JSONObject) ob;

		long totalresults = (long) jsonObj.get("TotalResults");
		logger.debug("Totals Results: "+totalresults);
		
		
		JSONArray results = (JSONArray) jsonObj.get("Results");

		for(int i = 0; i < 1; i++)
		{
			JSONObject obj = (JSONObject) results.get(i);
			product_id = (String) obj.get("ProductId");
		}

		return (long) totalresults;
	}

	private static String normalizeCsv(String val) {

		if (val == null || val.trim().length() == 0 || val.trim().equalsIgnoreCase("null")) {
			return "";
		}

		StringBuffer sb = new StringBuffer();

		// replace newline characters
		String nval = val.trim().replaceAll("\r?\n", " ");
		// replace tab character
		nval = nval.replaceAll("\t", " ");
		nval = nval.replaceAll("\r", " ");
		nval = nval.replaceAll("\n", " ");

		// escape entire value with quote character '"'
		sb.append(quoteChar);
		//check if string contains any special character which has to be escaped
		if(nval.indexOf(quoteChar) != -1)
			sb.append(processLine(nval));
		else
			sb.append(nval);

		sb.append(quoteChar);

		return sb.toString();
	}

	protected static StringBuffer processLine(String nextElement) {

		StringBuffer sb = new StringBuffer();
		for (int j = 0; j < nextElement.length(); j++) {
			char nextChar = nextElement.charAt(j);
			if (nextChar == quoteChar) {
				sb.append(quoteChar).append(nextChar);
			} else {
				sb.append(nextChar);
			}
		}

		return sb;
	}

}
