package com.csscorp.portal.ecommerce;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csscorp.properties.SeagateHelper;
import com.csscorp.properties.SeagateProperties;

public class EBay {

	private static Logger logger = LoggerFactory.getLogger(EBay.class);
	private static final SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
	private static final SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");

	public EBay() throws IOException {
		logger.info("------------ Seagate EBAY Web Scraping :: [Start] :: --------------");
		long startTime = System.nanoTime();
		webScraper();
		long endTime   = System.nanoTime();
		long totalTime = endTime - startTime;
		logger.info("Elapsed time in milliseconds: " + totalTime / 1000000);
		logger.info("------------ Seagate EBAY Web Scraping :: [End] :: --------------");
	}

	private static void webScraper() throws IOException {
		
		String productCategory = SeagateProperties.getEBayProduct();
		String strUrl = SeagateProperties.getEBayURL();
		String strFolderPath = SeagateProperties.getEBayOutputPath();
		String strProjectName = SeagateProperties.getEBayProject();

		int pageId = 0;
		
		String reviewDate = null;
		
		int startRange = 1;

		String strFilePath = null;

		String strTimeStamp = timeStampFormat.format(new Date());

		String directoryName = strFolderPath + strProjectName;
		logger.debug("directoryName: " + directoryName);
		SeagateHelper.createFolders(directoryName);
		strFilePath = directoryName + "//" + productCategory + "_ebay_" + strTimeStamp + ".tsv";
		BufferedWriter buffWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(strFilePath)));
		
		String url = strUrl.substring(0, strUrl.lastIndexOf("&"));
		logger.info("URL: " + url);
		Document firstDoc = null;

		firstDoc = Jsoup.connect(url).timeout(0).get();
		Elements headerElements = firstDoc.select("h2[class=reviews-section-title]");
		String[] maxPageStr = headerElements.text().split(" ");
		int totalReviews = Integer.parseInt(maxPageStr[0]);
		logger.info("Total Reviews: " + totalReviews);
		
		int maxPages = (totalReviews / 10) + 1;
		//int max_pages = 2;
		pageId = maxPages;
		logger.info("Max Pages: " + maxPages);
		
		try{
			for (int i = startRange; i <=maxPages; i++) {
				
				url = strUrl + i;
				logger.info("URL: " + url);
				Document document = null;
				
				document = Jsoup.connect(url).timeout(0).get();
				Elements elements = document.select("div[class=reviews]");
				
				for (Element element : elements) {
										
					Elements reviewSection = element.getElementsByAttributeValue("class", "ebay-review-section");
					for (Element reviews : reviewSection) {
						
						Elements names = reviews.getElementsByAttributeValue("itemprop", "name");
						String name = names.text();
						//logger.debug("Names: " + name);
						
						Elements reviewBodys = reviews.getElementsByAttributeValue("itemprop", "reviewBody");
						String reviewBody = reviewBodys.text().replace("Read full review...", "");
						//logger.debug("Review Body: " + reviewBody);
						
						Elements authors = reviews.getElementsByAttributeValue("itemprop", "author");
						String author = authors.text();
						//logger.debug("Author: " + author);
						
						Elements datesPublished = reviews.getElementsByAttributeValue("itemprop", "datePublished");
						String datePublished = datesPublished.text();

						try {
							Date date = sdf.parse(datePublished);
							reviewDate = output.format(date);
						} catch (ParseException exception) {
							logger.error("Exception: ", exception);
						}
						//logger.debug("Date Published: " + reviewDate);
						
						Elements ratingVals = reviews.select("meta");
						String ratingInt = ratingVals.attr("content");
						//System.out.println("Rating Value: " + ratingInt);
						
						logger.debug("ebay.com" + "\t" + productCategory + "\t" + name + "\t" + reviewDate + "\t" + author + "\t" + reviewBody + "\t" + ratingInt + "\n");
						buffWriter.write("ebay.com" + "\t" + i + "\t" + pageId + "\t" + productCategory + "\t" + name + "\t" + reviewDate + "\t" + author + "\t" + reviewBody + "\t" + ratingInt + "\n");
						
						/*
						Elements starRating = element.getElementsByAttributeValue("class", "ebay-star-rating");
						
						for (Element star : starRating) {
							Elements ratingVals = star.select("meta");
							System.out.println("Rating Value: " + ratingVals.attr("content"));

							System.out.println("-----------------------");
						}
						*/

					}
				}
				
				/*
				for (Element element : elements) {
					
					Elements names = element.getElementsByAttributeValue("itemprop", "name");
					for (Element name : names) {
						System.out.println("Name: " + name.text());
						System.out.println("-----------------------");
					}
					
					System.out.println("###########################");
					
					Elements reviewBodys = element.getElementsByAttributeValue("itemprop", "reviewBody");
					for (Element reviewBody : reviewBodys) {
						System.out.println("Review Body: " + reviewBody.text().replace("Read full review...", ""));
						System.out.println("-----------------------");
					}

					System.out.println("###########################");
					
					Elements authors = element.getElementsByAttributeValue("itemprop", "author");
					for (Element author : authors) {
						System.out.println("Author: " + author.text());
						System.out.println("-----------------------");
					}

					System.out.println("###########################");
					
					Elements datePublisheds = element.getElementsByAttributeValue("itemprop", "datePublished");
					for (Element datePublished : datePublisheds) {
						System.out.println("Date Published: " + datePublished.text());
						System.out.println("-----------------------");
					}

					System.out.println("###########################");
					
					Elements ratingValues = element.getElementsByAttributeValue("class", "ebay-star-rating");
					
					for (Element ratingValue : ratingValues) {
						Elements ratingVals = ratingValue.select("meta");
						System.out.println("Rating Value: " + ratingVals.attr("content"));

						System.out.println("-----------------------");
					}

		        }
		        */
				pageId = pageId - 1;
			}				

		} catch (IOException exception) {
			logger.error("Exception: ", exception);
		}
		finally {
			try {
				buffWriter.close();
			} catch (IOException exception) {
				logger.error("Exception: ", exception);
			}
		}		
		
		/*
		 * baseElement.attr("class").equals("a-section review")
		 * String author = reviewEle.getElementsByAttributeValue("itemprop", "author").text();
		 * 
		 */
	}
}
