package com.csscorp.blogs;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csscorp.properties.SeagateHelper;
import com.csscorp.properties.SeagateProperties;

public class MouthShut {

	private static Logger logger = LoggerFactory.getLogger(MouthShut.class);
	private static final SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy hh:mm aaa");
	private static final SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");

	public MouthShut() throws IOException, ParseException {
		logger.info("------------ Seagate MOUTHSHUT Web Scraping :: [Start] :: --------------");
		long startTime = System.nanoTime();
		webScraper();
		long endTime   = System.nanoTime();
		long totalTime = endTime - startTime;
		logger.info("Elapsed time in milliseconds: " + totalTime / 1000000);
		logger.info("------------ Seagate MOUTHSHUT Web Scraping :: [End] :: --------------");
	}

	private static void webScraper() throws IOException, ParseException {
		
		String strUrl = SeagateProperties.getMouthShutURL();
		String strFolderPath = SeagateProperties.getMouthShutOutputPath();

		String strTimeStamp = timeStampFormat.format(new Date());

		String directoryName = strFolderPath;
		logger.debug("directoryName: " + directoryName);
		SeagateHelper.createFolderIfNotExists(directoryName);
		String strFilePath = directoryName + "//" + "Mouthshut_" + strTimeStamp + ".tsv";
		BufferedWriter buffWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(strFilePath)));
		
		int max_pages = SeagateProperties.getMouthShutMaxPages();
		
		for (int i = 1; i <= max_pages; i++){
			
			String url = strUrl;
			logger.info("URL: " + url);

			try{
				
				Document document = Jsoup.connect(url).timeout(0).get();
				Elements alldiv = document.select("div[class=row review-article]");
				
				for(Element reviewEle : alldiv) {

					Elements reviewContent = reviewEle.getElementsByAttributeValue("class", "rating");
					
					Element smallEle = reviewContent.select("small").first();
					
					String publishedDate = smallEle.getElementsByAttributeValueStarting("id", "ctl00_ctl00").first().text();
					
					if (publishedDate.indexOf("ago") >= 1){
						publishedDate = SeagateHelper.reverseParsePrettyDate(publishedDate);
					}
					else {
						Date date = sdf.parse(publishedDate);
						publishedDate = output.format(date);
					}
					logger.debug("Published Date: " + publishedDate);
						
					Elements profileContent = reviewEle.getElementsByAttributeValue("class", "more reviewdata");
					Element aEle = profileContent.select("a").first();
					String ratingContent = aEle.attr("onclick");

					String rating = SeagateHelper.ratingRegexExtractor(ratingContent);
					logger.debug("Rating: " + rating);
						
					String preContent = profileContent.text();
					String[] trimContent = preContent.split("...Read More");
					String content = trimContent[0].trim();
					logger.debug("Content: " + content);
					
					String author = "Not Available";
					buffWriter.write(author + "\t" + content + "\t" + rating + "\t" + publishedDate + "\n");
				}

		} catch (IOException exception) {
			logger.error("Exception: ", exception);
		}
		finally {
			try {
				buffWriter.close();
			} catch (IOException exception) {
				logger.error("Exception: ", exception);
			}
		}		
		
	}
 }
}