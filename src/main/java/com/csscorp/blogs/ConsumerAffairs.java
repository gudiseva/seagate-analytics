package com.csscorp.blogs;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csscorp.properties.SeagateHelper;
import com.csscorp.properties.SeagateProperties;

public class ConsumerAffairs {

	private static Logger logger = LoggerFactory.getLogger(ConsumerAffairs.class);
	private static final SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
	private static final SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");

	public ConsumerAffairs() throws IOException {

		logger.info("------------ Seagate CONSUMER AFFAIRS Web Scraping :: [Start] :: --------------");
		long startTime = System.nanoTime();
		webScraper();
		long endTime   = System.nanoTime();
		long totalTime = endTime - startTime;
		logger.info("Elapsed time in milliseconds: " + totalTime / 1000000);
		logger.info("------------ Seagate CONSUMER AFFAIRS Web Scraping :: [End] :: --------------");

	}
	
	private static void webScraper() throws IOException {

		String strLastUpdatedFileName = timeStampFormat.format(new Date());

		String directoryName = SeagateProperties.getConsumerAffairsOutputPath();
		SeagateHelper.createFolderIfNotExists(directoryName);
		String strLastUpdatedFilePath = directoryName + "ConsumerAffairs_" + strLastUpdatedFileName + ".tsv";
		BufferedWriter buffWriter = null;
		try {
			buffWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(strLastUpdatedFilePath)));
		} catch (FileNotFoundException exception) {
			logger.error("FileNotFoundException: ", exception);
		}

		String strURL = SeagateProperties.getConsumerAffairsURL();
		                 
		String url = null;

		int max_pages = SeagateProperties.getConsumerAffairsMaxPages(); // MAX PAGES = 2

		try {

			for (int i = 1; i <= max_pages; i++){

				url = strURL + i;
				logger.info("URL: " + url);

				Document doc = Jsoup.connect(url).timeout(0).get();

				Elements alldiv = doc.select("div[class=review review--user-post]");

				for(Element reviewEle : alldiv)
				{
					String author = reviewEle.getElementsByAttributeValue("itemprop", "author").text();
					String publishedDate = reviewEle.getElementsByAttributeValue("itemprop", "datePublished").text();

					publishedDate = publishedDate.replace(".","");
					publishedDate = publishedDate.replace("Sept","Sep");
					
					try {
						Date date = sdf.parse(publishedDate);
						publishedDate = output.format(date);
					} catch (ParseException exception) {
						logger.error("Exception: ", exception);
					}

					String rating;
					
					Element ratingEle = reviewEle.getElementsByAttributeValue("itemprop", "ratingValue").first();
					if (ratingEle == null){
						rating = "Not Rated";
					}
					else{
						rating = ratingEle.attr("content");	
					}
					
					
					String content = reviewEle.getElementsByAttributeValue("class", "non-brand-campaign clearfix").text();

					logger.debug(author + "\t" + content + "\t" + rating + "\t" + publishedDate);
					buffWriter.write(author + "\t" + content + "\t" + rating + "\t" + publishedDate + "\n");
					
				}
				
			}

		} catch (IOException exception) {
			logger.error("Exception: ", exception);
		}
		finally {
			try {
				buffWriter.close();
			} catch (IOException exception) {
				logger.error("Exception: ", exception);
			}
		}
	
	}

}
