package com.csscorp.blogs;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csscorp.properties.SeagateHelper;
import com.csscorp.properties.SeagateProperties;

public class HissingKitty {

	private static Logger logger = LoggerFactory.getLogger(HissingKitty.class);
	private static final SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
	private static final SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");

	public HissingKitty() throws IOException {

		logger.info("------------ Seagate HISSING KITTY Web Scraping :: [Start] :: --------------");
		long startTime = System.nanoTime();
		webScraper();
		long endTime   = System.nanoTime();
		long totalTime = endTime - startTime;
		logger.info("Elapsed time in milliseconds: " + totalTime / 1000000);
		logger.info("------------ Seagate HISSING KITTY Web Scraping :: [End] :: --------------");

	}

	private void webScraper() throws IOException {

		String strLastUpdatedFileName = timeStampFormat.format(new Date());

		String directoryName = SeagateProperties.getHissingKittyOutputPath();
		SeagateHelper.createFolderIfNotExists(directoryName);
		String strLastUpdatedFilePath = directoryName + "HissingKitty_" + strLastUpdatedFileName + ".tsv";
		BufferedWriter buffWriter = null;
		try {
			buffWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(strLastUpdatedFilePath)));
		} catch (FileNotFoundException exception) {
			logger.error("FileNotFoundException: ", exception);
		}

		String strURL = SeagateProperties.getHissingKittyURL();
		
		String url = null;
		
		int max_pages = SeagateProperties.getHissingKittyMaxPages(); // MAX PAGES = 0

		try {

			for(int p = 0; p <= max_pages; p++)
			{
				// skipping page 2 because page 1 and page 2 have same content
				if(p == 2)
					continue;
			
				url = strURL + p;
				logger.info("URL: " + url);

				Document doc = Jsoup.connect(url).timeout(0).get();

				Element alldiv = doc.select("div[class=view-content]").first();

				try{
					
					Elements baseElements = alldiv.getElementsByAttributeValue("class", "field-content");

					for(Element baseElement : baseElements){
						
						String author = baseElement.getElementsByAttributeValue("itemprop", "author").text();
						if (author.length() == 0) author = "NA";
						String publishedDate = baseElement.getElementsByAttributeValue("itemprop", "datePublished").text();

						Element ratingEle = baseElement.getElementsByAttributeValue("itemprop", "ratingValue").first();
						String rating = ratingEle.attr("content");

						String content = baseElement.getElementsByAttributeValue("class", "body-content p-20").text();

						publishedDate = publishedDate.replace(".","");
						publishedDate = publishedDate.replace("Sept","Sep");
						
						try {

							Date date = sdf.parse(publishedDate);
							publishedDate = output.format(date);
						} catch (ParseException exception) {
							logger.error("Exception: ", exception);
						}

						logger.debug(author + "\t" + content + "\t" + rating + "\t" + publishedDate);
						buffWriter.write(author + "\t" + content + "\t" + rating + "\t" + publishedDate + "\n");

					}
					
				} catch (NullPointerException exception) {
					logger.error("NullPointerException: ", exception);
					break;
				}
					
			}

		} catch (IOException exception) {
			logger.error("IOException: ", exception);
		}
		finally {
			try {
				buffWriter.close();
			} catch (IOException exception) {
				logger.error("Exception: ", exception);
			}
		}
		
	}

}
