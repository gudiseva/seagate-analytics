package com.csscorp.app;

import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csscorp.blogs.ConsumerAffairs;
import com.csscorp.blogs.HissingKitty;
import com.csscorp.blogs.MouthShut;
import com.csscorp.portal.ecommerce.Amazon;
import com.csscorp.portal.ecommerce.BestBuy;
import com.csscorp.portal.ecommerce.EBay;

public class MainApp {

	private static Logger logger = LoggerFactory.getLogger(MainApp.class);
	
	public static void main(String[] args) {

		logger.info("------------ Seagate Web Scraping :: [Start] :: --------------");
		
		// check if the length of args array is greater than 0
        if (args.length > 0) {
        	
        	logger.info("Command Line Arguments :: Available :: Value Accepted [Amazon, BestBuy, eBay, ConsumerAffairs, HissingKitty] only ...");
    		String inputValue = args[0].toLowerCase();
	    	logger.info("Input value: " + inputValue);
			
	    	switch(inputValue){  
			    case "amazon": {
					try {
						new Amazon();
					} catch (IOException exception) {
						logger.error("Exception: ", exception);
					}
			    	break;  
			    }
			    case "bestbuy": {
					try {
						new BestBuy();
					} catch (IOException exception) {
						logger.error("Exception: ", exception);
					} catch (ParseException exception) {
						logger.error("Exception: ", exception);
					}
			    	break;    
			    }
			    case "ebay": {
					try {
						new EBay();
					} catch (IOException exception) {
						logger.error("Exception: ", exception);
					}
			    	break;  
			    }
			    case "mouthshut": {
					try {
						new MouthShut();
					} catch (IOException exception) {
						logger.error("Exception: ", exception);
					} catch (java.text.ParseException exception) {
						logger.error("Exception: ", exception);
					}
			    	break;  
			    }
			    case "consumeraffairs": {
					try {
						new ConsumerAffairs();
					} catch (IOException exception) {
						logger.error("Exception: ", exception);
					}
			    	break;    
			    }
			    case "hissingkitty": {
					try {
						new HissingKitty();
					} catch (IOException exception) {
						logger.error("Exception: ", exception);
					}
			    	break;    
			    }
			    default: {
			    	logger.error("Invalid Value!!! :: Not in [Amazon, BestBuy, eBay, ConsumerAffairs, HissingKitty]");
			    	System.exit(-1);
			    }  
		    }  

        } else {
        	
        	logger.info("Command Line Arguments :: Unavailable :: Program Exiting ...");
	        System.exit(-1);

        }
		
        logger.info("------------ Seagate Web Scraping :: [End] :: --------------");
        
	}

}
